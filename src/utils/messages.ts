export const M_WHO: string[] = [
  'štrumpf i štrumpfeta',
  'Kerovi na motori',
  'dvojica',
  'jedan dojavljuje drugi čeka',
  'Narko kombi',
  'dvojac pandura',
  'Zeleni Punto',
  'bela škoda',
];


export const M_WHERE: string[] = [
  'kod Šojku',
  'na Trošarinu',
  'Apelovac',
  'Dušanova',
  'izlaz sever',
  'izlaz jug',
  'Deki roštilj',
  'kod elektronsku',
  'staro groblje',
  'po Pantelej',
  'kod nadvoznjak'
];

export const M_WHAT: string[] = [
  'radar',
  'zaustavljaju',
  'rasipuju',
  'deru ne praštaju',
  'izuvaju iz cipele',
  'skidaju čarape',
  'svetla i pojas',
  'sakrili se',
  'šunjaju se',
  'duvaljka',
  'terorišu',
];

export const M_SPICE: string[] = [
  'usta ih',
  'kao nindže su',
  'nema beganje',
  'redaljka',
];

export const SENTENCE_TYPES: string[][][] = [
  [M_WHERE, M_WHAT],
  [M_WHO, M_WHERE, M_WHAT ],
  [M_WHERE, M_WHAT, M_SPICE],
  [M_WHO, M_WHERE, M_WHAT, M_SPICE],
];