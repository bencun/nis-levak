export const M_WHO_MALE: string[] = [
  'Deda',
  'Kum',
  'Stric',
  'Komsija',
  'Brat',
  'Prijatelj',
];

export const M_WHO_FEMALE: string[] = [
  'Kumica',
  'Strina',
  'Komsika',
  'Baba',
  'Prija',
  'Sestra',
];

export const M_WHO_SPICE: string[] = [
  'i njihove mnogobrojne porodice ',
  'i njihove mnogobrojne rodnine ',
  'i cela rodbina',
  'i cela porodica',
];



export const M_WHO_NAME_FEMALE: string[] = [
  'Stanimirka',
  'Žitomirka',
  'Ljubinka',
  'Velimirka',
  'Stanimirka',
];

export const M_WHO_NAME_MALE: string[] = [
  'Stanko',
  'Žitomir',
  'Ljubivoje',
  'Srdjan',
  'Velimir',
  'Stanimir',
  'Mihajlo',
  'Milosav', 
  'Milutin',
];

export const M_WHAT: string[] = [
  'cestita',
  'zeli srecno',
];

export const M_WHERE: string[] = [
  'iz sela Zitkovac',
  'iz sela Vrničani kod Čačka',
  'iz Ugrinovaca',
  'iz inostranstva',
];


export const M_WHOM: string[] = [
  'kumcetu',
  'pasenogu',
  'sinovcu',
  'teci',
  'tetki',
  'cici',
  'strini',
  'svekrvi',
];

export const M_WHY: string[] = [
  'betoniranje dvorista',
  'telenje krave',
  'ugradnja kuka',
  '13. rođendan',
  'odlazak u vojsku',
  'operaciju očiju',
  'rodjenje sina',
  'rodjenje unuka',
  'rodjenje unuke',
  'kupovinu auta',
  'kupovinu kuce',
  'zavrsetak fasade',
  'preseljenje u drugu kuću',
  'obrezivanje',
  'odlazak u penziju',
  'sklapanje braka',
  'useljenje u novosagrađenu kuću',
];

export const M_SONG: string[] = [
  'Odrasti ruzo moja',
  'Da nam zivis jos toliko',
  'Oj Žiko, ljubi l tvoja mala, te tvoje vite, noge od metala',
  'PRVI SAM TE LJUBIO NA TRAVI',
  'Odrastao sam sa ovcama',
  'TRI BUNARA JEDNA KOFA SLUŽI',
  'SIĐI MUNJO UDRI GROME',
  
];

export const M_SPICE: string[] = [
  ', ziveli',
  ', srecno',
  ', ljubim',
  ', pozdrav',
  ', sve najbolje',
];

export const M_WHICH: string[] = [
  'pesmom',
];



export const SENTENCE_TYPES: string[][][] = [
  [M_WHO_MALE, M_WHO_NAME_MALE, M_WHERE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG, M_SPICE],
  [M_WHO_MALE, M_WHO_NAME_MALE, M_WHERE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG],
  [M_WHO_MALE, M_WHO_NAME_MALE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG, M_SPICE],
  [M_WHO_MALE, M_WHO_NAME_MALE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG],
  [M_WHO_FEMALE, M_WHO_NAME_FEMALE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG, M_SPICE],
  [M_WHO_FEMALE, M_WHO_NAME_FEMALE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG],
  [M_WHO_FEMALE, M_WHO_NAME_FEMALE, M_WHERE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG, M_SPICE],
  [M_WHO_FEMALE, M_WHO_NAME_FEMALE, M_WHERE, M_WHAT, M_WHY, M_WHOM, M_WHICH, M_SONG],
];