import {SENTENCE_TYPES} from './messages';
import {SENTENCE_TYPES as POZDRAVI} from './messages2';

const randomIdx = <T>(a: T[]) => Math.floor(Math.random() * a.length);
const randomEl = <T>(a: T[]) => a[randomIdx(a)];

export default function(pozdravi?: boolean) {
  const sentenceType = pozdravi ? randomEl(POZDRAVI) : randomEl(SENTENCE_TYPES);

  const sentenceElements = sentenceType.map(x => randomEl(x));

  return sentenceElements.join(' ')
};