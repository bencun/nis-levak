import { useState } from "react";
import { Outlet, Link } from "react-router-dom";
import generator from "./utils/generator";

function App() {
  const currentTime = new Date();
  const hours = currentTime.getHours();
  const minutes = currentTime.getMinutes();
  const hstr = hours > 9 ? hours : `0${hours}`;
  const mstr = minutes > 9 ? minutes : `0${minutes}`;

  const [message, setMessage] = useState(generator());

  const generateMessage = () => {
    setMessage(generator())
  }

  return (
    <div className="h-full w-full flex justify-center items-center flex-col p-4">
      <Link to={`pozdravi`} className="bg-slate-700 p-2 my-4 border border-black flex">Pozdravi i zelje</Link>
      <div className="bg-slate-700 p-4 border border-black flex">
        {/* avatar */}
        <div className="relative rounded-full overflow-hidden w-10 h-10 mr-4 flex justify-center items-center shrink-0">
          <img src="/levak.jpeg" className="inline-block w-10 h-10"/>
        </div>
        {/* content */}
        <div>
          {/* title */}
          <div className="mb-1"><h1>NIŠ-LEVAK-NIŠ&nbsp;&nbsp;&nbsp;&nbsp;🔕</h1></div>
          {/* message content */}
          <div className="max-w-screen-sm">{message}</div>
        </div>
        {/* time and number of new messages */}
        <div className="flex flex-col px-2 items-center font-normal text-sm">
          <div className="mb-2">{`${hstr}:${mstr}`}</div>
          <div className="rounded-full bg-gray-300 px-2">999+</div>
        </div>
      </div>

      {/* refresh button */}
      <button className="p-4 text-3xl cursor-pointer" onClick={generateMessage}>
        🔄 🚔
      </button>
    </div>
  );
}

export default App
